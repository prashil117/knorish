import React, { Component } from "react";
import { Provider } from "react-redux";
import { ThemeProvider } from "react-native-elements";
import AppContainer from "./app/navigation";
import store from "./app/reducers";
import theme from "./app/theme";
import navigationService from "./app/service/navigationService";
import SplashScreen from 'react-native-splash-screen'
import { render } from "react-dom";

export default class App extends React.Component {
  componentDidMount() {
    //setTimeout(() => {
      SplashScreen.hide()
    // }, 300)  
      // console.warn('hide splash called');
}

  render(){
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <AppContainer
          ref={(navigatorRef) => {
            navigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    </ThemeProvider>
  )
        }
}
