import React from 'react';
import { connect } from 'react-redux';
import { ActivityIndicator } from 'react-native';
import ScreenContainer from '../ScreenContainer';
import store from '../reducers';
import checkAsyncStorage from '../helpers/checkAsyncStorage';
import AsyncStorage from '@react-native-community/async-storage';
import { loggedIn } from '../actions/auth';
import navigationService from '../service/navigationService';
import { refreshTokenUrl } from '../constants/auth';
const jwt_decode = require('jwt-decode');

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this.bootstrapAsync();
  }

  bootstrapAsync = async () => {
    
    const userStorage = await checkAsyncStorage();
    
    await store.dispatch(this.props.loggedIn({
      user: userStorage.user,
      token: userStorage.token,
    }));
    
    if(userStorage.token){
      if(userStorage.token && ! await this.isTokenExpired(userStorage.token)) {
        navigationService.navigate('App', {});
        return;
      }else {
        const isRefreshSuccess = await this.getRefreshTokenFromApi(userStorage.token);
        if (!isRefreshSuccess) {
           navigationService.navigate('Auth', {});
        }
        if(isRefreshSuccess) {
          navigationService.navigate('App', {});
       }
      }
    }else {
      navigationService.navigate('Auth', {});  
    }
  };

  render() {
    return (
      <ScreenContainer>
        <ActivityIndicator />
      </ScreenContainer>
    );
  }

  async isTokenExpired(token) {
    let decodedToken = jwt_decode(token);
    let currentDate = new Date();
  
    // JWT exp is in seconds
    if (decodedToken.exp * 1000 < currentDate.getTime()) {
      return true;
    } else {
      result = false;
    }
  }

  async getRefreshTokenFromApi(token) {
    // console.warn(`USer name = ${username} password=${password}`)
    const refreshToken = AsyncStorage.getItem('refereshToken');

    if (!token || !refreshToken) {      
      return false;
    }

    var myHeaders = new Headers();

    myHeaders.append("Content-Type", "application/json");

    const credentials = JSON.stringify({ accessToken: token, refreshToken: refreshToken });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: credentials,
      redirect: 'follow'
    };

    fetch(
      `${refreshTokenUrl}`,
      requestOptions)
      .then(response => response.json())
      .then(result => {
        // console.warn(result)
        if(result.status)
        {
          AsyncStorage.setItem('userData', JSON.stringify(result.UserName) )
          AsyncStorage.setItem('refereshToken', JSON.stringify(result.refereshToken) )
          AsyncStorage.setItem('refereshTokenExpiryTime', JSON.stringify(result.refereshTokenExpiryTime) )
          AsyncStorage.setItem('authToken', result.authToken)

          return true;
        }
        else
        {
          alert(result.error);
        }
     })
      .catch(error => {
        return false
      });
  }
}

function mapStateToProps(state) {
  return { loggingIn: state.auth.loggingIn };
}



export default connect(mapStateToProps, { loggedIn })(AuthLoadingScreen);
