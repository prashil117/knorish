import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Input } from 'react-native-elements';
import {
  ActivityIndicator, Alert, View, SafeAreaView, Modal,
  StyleSheet,
  ScrollView,
  Text,
  StatusBar,
  Dimensions,
  Image,
  Linking
} from 'react-native';
import { clearLoginErrorMessage, login,forgo } from '../../actions/auth';
import navigationService from '../../service/navigationService';
import AsyncStorage from '@react-native-community/async-storage';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { tanetId, loginUrl,forgotPasswordUrl } from '../../constants/auth';
import Spinner from 'react-native-loading-spinner-overlay';

class LoginForm extends Component {
  state = {
    username: '',//'abhimanyu@knorish.com',
    password: '',//'hellodemo',
    isUserNameFocused: false,
    isPasswordNameFocused: false,

  };
  constructor(props) {
    super(props);
    this.state = {
      screen: Dimensions.get('window'),
      showSpinner: false,
    };
  }

  getOrientation() {
    if (this.state.screen.width > this.state.screen.height) {
      return 'LANDSCAPE';
    } else {
      return 'PORTRAIT';
    }
  }

  getStyle() {
    if (this.getOrientation() === 'LANDSCAPE') {
      return landscapeStyles;
    } else {
      return portraitStyles;
    }
  }

  showSpinner() {
    this.setState({ showSpinner: true });
  }

  hideSpinner() {
    this.setState({ showSpinner: false });
  }

  onLayout() {
    this.setState({ screen: Dimensions.get('window') });
  }

  async getTokenFromApi(username, password) {
    
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!username) {
      alert('Please Enter Email');
      return
    }
    // else if (reg.test(username) === false) {
    //   alert("Please enter valid email");
    //   return
    // }
    else if (!password) {
      alert('Please Enter Password!');
      return
    }

    var myHeaders = new Headers();

    myHeaders.append("Content-Type", "multipart/form-data");
    this.showSpinner();

    var formdata = new FormData();
    formdata.append("tenantId", `${tanetId}`);
    formdata.append("userName", username);
    formdata.append("password", password);
    formdata.append("deviceID", "android");
    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: formdata,
      redirect: 'follow'
    };

    await fetch(
      `${loginUrl}`,
      requestOptions)
      .then(response => response.json(),this.hideSpinner())
      .then(result => {
        
        if(result.status)
        {
          AsyncStorage.setItem('userData', JSON.stringify(result.UserName) )
          AsyncStorage.setItem('refereshToken', JSON.stringify(result.refereshToken) )
          AsyncStorage.setItem('refereshTokenExpiryTime', JSON.stringify(result.refereshTokenExpiryTime) )
          AsyncStorage.setItem('authToken', result.authToken)
          navigationService.navigate('App');
        }
        else {
          console.log("eerr",result.error)
          alert(result.error);
        }
      })
      .catch(error => {
        this.hideSpinner();
        alert('Please enter valid username and password.')
      });
  }

  signInAsync = async () => {
    const { password, username } = this.state;
    // await this.props.login(username, password);
    await this.getTokenFromApi(username.trim(), password);
    // navigationService.navigate('App');
  };
  
  render() {
    const { loggingIn } = this.props;
    const { username, password } = this.state;
    const { height, width } = Dimensions.get('screen')
    // const isEnabledSubmit = (username.length >= 4 && password.length >= 5);
    return (

      <SafeAreaView style={{
        ...styles.container,
        justifyContent: 'center',
      }} onLayout={this.onLayout.bind(this)}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          scrollEnabled={this.getOrientation() == "LANDSCAPE" ? true : false}
          style={{ width: '100%', flex: 1 }}
          contentContainerStyle={{
            flexGrow: 1, justifyContent: this.getOrientation() == "LANDSCAPE" ? 'flex-start' : 'center', alignItems: 'center',
            paddingBottom: this.getOrientation() == "LANDSCAPE" ? 100 : 0,
            paddingTop: this.getOrientation() == "LANDSCAPE" ? 100 : 0,

          }}
        >
            <View style={styles.ImageLogo}>
            <Image  source={require("../../Assets/logo.png")}/>
            </View>
          

          <View style={styles.LoginContainer}>
            <View style={styles.LoginHeader}>

              <Text style={{ color: 'white', fontSize: 25 }}>
                Login
         </Text>
            </View>

            <TextInput style={{ ...styles.InputField, borderBottomColor: this.state.isUserNameFocused ? '#9036AA' : 'rgb(212,212,212)', }}
              autoCapitalize="none"
              placeholder="Email"
              onChangeText={usr => this.setState({ username: usr })}
              value={username}
              onFocus={(field) => {
                this.setState({ isUserNameFocused: true, isPasswordNameFocused: false })
              }}
            />
            <TextInput style={{ ...styles.InputField, borderBottomColor: this.state.isPasswordNameFocused ? '#9036AA' : 'rgb(212,212,212)', }}
              placeholder="Password"
              onChangeText={pass => this.setState({ password: pass })}
              secureTextEntry
              onFocus={(field) => {
                this.setState({ isUserNameFocused: false, isPasswordNameFocused: true })
              }}
            />
            <TouchableOpacity style={styles.SubmitContainer}
              // disabled={!isEnabledSubmit}
              onPress={() => {
                this.signInAsync()
              }}
            >
              <Text style={{ width: '80%', padding: 15, paddingLeft: 45, paddingRight: 45, color: 'white', fontWeight: '600', fontSize: 17 }}>
                Submit
         </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.ForgotPasswordContainer}
              // disabled={!isEnabledSubmit}
              onPress={() => Linking.openURL(forgotPasswordUrl)}
            >
              <Text style={{ width: '50%', padding: 1, paddingLeft: 5, paddingRight: 5, color: '#d24470', fontWeight: '600', fontSize: 14 }}>
                Forgot Password?
              </Text>
            </TouchableOpacity>
          </View>
          <Spinner
            visible={this.state.showSpinner}
            textContent={'Please Wait...'}
            textStyle={{ color: '#fff' }}
            color={'#d24470'}
          />
          
        </ScrollView>
      </SafeAreaView>
    );
  }
}

LoginForm.propTypes = {
  errorMessage: PropTypes.string.isRequired,
  loggingIn: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    loggingIn: state.auth.loggingIn,
    errorMessage: state.auth.errorMessage,
  };
}
const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  modalContainer: {
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#1E3643'
  },
  ImageLogo: {
    top: -30,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 50,
  },
  LoginContainer: {
    paddingTop: 40,
    paddingBottom: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    width: '90%',
    borderRadius: 15,
  },
  InputField: {
    marginTop: 20,
    width: '80%',
    height: 45,
    borderBottomColor: 'rgb(212,212,212)',
    borderBottomWidth: 1,
  },
  SubmitContainer:
  {
    marginTop: 50,
    backgroundColor: '#d24470',
    width: '80%',
    borderRadius: 10,
  },
  ForgotPasswordContainer:
  {
    marginTop: 10,
    borderColor: '#d24470',
    borderWidth: 1,
    width: '50%',
    borderRadius: 2,
    height: 25
  },
  LoginHeader: {
    height: 60,
    backgroundColor: '#d24470',
    width: '90%',
    position: 'absolute',
    top: -30,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',

  }


});
export default connect(mapStateToProps, {
  login,
  clearLoginErrorMessage,
})(LoginForm);
