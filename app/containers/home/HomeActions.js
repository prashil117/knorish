import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-native';
import navigationService from '../../service/navigationService';
import { logout } from '../../actions/auth';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { WebView, WebViewNavigation } from 'react-native-webview';
import { baseURL, comapreUrlLogout } from '../../constants/auth';
import Spinner from 'react-native-loading-spinner-overlay';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? StatusBar.currentHeight : StatusBar.currentHeight;

class HomeActions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      authToken: '',
      html: '',
      screen: Dimensions.get('window'),
      showSpinner: false,
    };

  };
  showSpinner() {
    this.setState({ showSpinner: true });
  }

  hideSpinner() {
    this.setState({ showSpinner: false });
  }
  getOrientation() {
    if (this.state.screen.width > this.state.screen.height) {
      return 'LANDSCAPE';
    } else {
      return 'PORTRAIT';
    }
  }

  getStyle() {
    if (this.getOrientation() === 'LANDSCAPE') {
      return landscapeStyles;
    } else {
      return portraitStyles;
    }
  }
  onLayout() {
    this.setState({ screen: Dimensions.get('window') });
  }
  async componentDidMount() {
    this.showSpinner();
    this.setState({ authToken: await AsyncStorage.getItem('authToken') })
    setTimeout(() => {
      const data = JSON.stringify({
        type: 'car',
        plate_number: 'c123'
      });
      fetch(`${baseURL}?Authorization=Bearer ${this.state.authToken}`, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${this.state.authToken}`,
          "Content-Type": "application/json"
        },
        body: data,
      }).then(response => response.text()).then(text => {
        this.setState({ html: text });
        this.hideSpinner();
      });

    }, 1000)
  }


  signOutAsync = async () => {
    await this.props.logout();
    navigationService.navigate('Auth');
  };


  render() {
    return (
      <SafeAreaView style={styles.container} onLayout={this.onLayout.bind(this)}>

        <View style={{

          width: this.getOrientation() == "LANDSCAPE" ? Dimensions.get('window').width : Dimensions.get('window').width,
          height: this.getOrientation() == "LANDSCAPE" ? Dimensions.get('window').height : Dimensions.get('window').height,
        }}
        >
          <Spinner
            visible={this.state.showSpinner}
            textContent={'Loading...'}
            textStyle={{ color: '#fff' }}
          />

          <WebView
            source={{
              html: this.state.html,
              baseUrl: `${baseURL}?Authorization=Bearer ${this.state.authToken}`,
            }}
            ignoreSslError={true}
            originWhitelist={['*']}
            onLoadStart={(navState) => {
              if (navState.nativeEvent.url === comapreUrlLogout) {
                try {
                  AsyncStorage.clear()
                  navigationService.navigate('Auth');
                }
                catch (exception) {
                }
              }
            }
            }
            onRequest={req => req.addHeader('Authorization', `Bearer ${this.state.authToken}`)}
            sharedCookiesEnabled={true}
            javaScriptEnabled={true}
            startInLoadingState={true}
            automaticallyAdjustContentInsets
            autoManageStatusBarEnabled
            domStorageEnabled={true}
/>
        </View>
      </SafeAreaView>
    );
  }
}

HomeActions.propTypes = {};

function mapStateToProps() {
  return {};
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1E3643',

  },

  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
});
export default connect(mapStateToProps, { logout })(HomeActions);
